import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WebsocketModule } from './websocket/websocket.module';
import { WebsocketGateaway } from './websocket/websocket.adapter';

@Module({
  imports: [WebsocketModule],
  controllers: [AppController],
  providers: [AppService, WebsocketGateaway],
})
export class AppModule {}
