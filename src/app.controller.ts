import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { WebsocketGateaway } from './websocket/websocket.adapter';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private ws: WebsocketGateaway,
  ) {}

  @Get()
  getHello() {
    return {
      status: 'ok',
      api_version: '1.0.0',
    };
  }

  @Post('addlist')
  async addList(@Body() data) {
    this.ws.server.emit('update_list', data);

    return {
      status: 'ok',
    };
  }

  @Post('updatePrice')
  async updatePrice(@Body() data) {
    this.ws.server.emit('update_price', data);

    return {
      status: 'ok',
    };
  }
}
