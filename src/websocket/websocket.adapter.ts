import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@WebSocketGateway()
export class WebsocketGateaway {
  @WebSocketServer()
  server;
  anonymous = 0;
  i = 1;

  afterInit(server) {
    console.log(`server websocket online`);
  }
  handleConnection(client) {
    this.anonymous++;
    console.log(`anonymous users ${this.anonymous}`);
  }

  handleDisconnect(client) {
    this.anonymous--;
    console.log(`anonymous users ${this.anonymous}`);
  }

  @SubscribeMessage('update_price')
  onUpdatePrice(client, data) {
    this.server.emit('update_price', data);
  }

  @SubscribeMessage('update_list')
  onUpdateList(client, data) {
    this.server.emit('update_list', data);
  }
}
