import { Module } from '@nestjs/common';
import { WebsocketGateaway } from './websocket.adapter';

@Module({
  providers: [],
})
export class WebsocketModule {}
