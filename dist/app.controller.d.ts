import { AppService } from './app.service';
import { WebsocketGateaway } from './websocket/websocket.adapter';
export declare class AppController {
    private readonly appService;
    private ws;
    constructor(appService: AppService, ws: WebsocketGateaway);
    getHello(): {
        status: string;
        api_version: string;
    };
    addList(data: any): Promise<{
        status: string;
    }>;
    updatePrice(data: any): Promise<{
        status: string;
    }>;
}
