"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const websockets_1 = require("@nestjs/websockets");
let WebsocketGateaway = class WebsocketGateaway {
    constructor() {
        this.anonymous = 0;
        this.i = 1;
    }
    afterInit(server) {
        console.log(`server websocket online`);
    }
    handleConnection(client) {
        this.anonymous++;
        console.log(`anonymous users ${this.anonymous}`);
    }
    handleDisconnect(client) {
        this.anonymous--;
        console.log(`anonymous users ${this.anonymous}`);
    }
    onUpdatePrice(client, data) {
        this.server.emit('update_price', data);
    }
    onUpdateList(client, data) {
        this.server.emit('update_list', data);
    }
};
__decorate([
    websockets_1.WebSocketServer(),
    __metadata("design:type", Object)
], WebsocketGateaway.prototype, "server", void 0);
__decorate([
    websockets_1.SubscribeMessage('update_price'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], WebsocketGateaway.prototype, "onUpdatePrice", null);
__decorate([
    websockets_1.SubscribeMessage('update_list'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], WebsocketGateaway.prototype, "onUpdateList", null);
WebsocketGateaway = __decorate([
    websockets_1.WebSocketGateway()
], WebsocketGateaway);
exports.WebsocketGateaway = WebsocketGateaway;
//# sourceMappingURL=websocket.adapter.js.map