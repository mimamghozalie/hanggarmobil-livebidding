export declare class WebsocketGateaway {
    server: any;
    anonymous: number;
    i: number;
    afterInit(server: any): void;
    handleConnection(client: any): void;
    handleDisconnect(client: any): void;
    onUpdatePrice(client: any, data: any): void;
    onUpdateList(client: any, data: any): void;
}
